<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Client'], function(){
    Route::get('/', 'HomeController@index');
    Route::get('perawatan', 'HomeController@perawatan');
    Route::get('perawatan/{slug}', 'HomeController@detailPerawatan');
    Route::get('perawatan/book-now/{id}', 'HomeController@formBook');
    Route::post('perawatan/book-now', 'ReservationController@bookNow');
    Route::get('reservation-on-proccess', 'ReservationController@proccess');

    // ROUTE ADD BABY in RESERVATION
    Route::get('myprofile/new-baby/{id}', 'ProfileController@newBabyR');


    Route::get('myprofile', 'ProfileController@myProfile');
    Route::get('myprofile/edit', 'ProfileController@editProfile');
    Route::put('myprofile/{id}/edit', 'ProfileController@updateProfile');
    Route::get('myprofile/new-baby', 'ProfileController@newBaby');
    Route::post('myprofile/new-baby', 'ProfileController@storeBaby');
    Route::get('myprofile/baby/{id}/edit', 'ProfileController@editBaby');
    Route::put('myprofile/baby/{id}/edit', 'ProfileController@updateBaby');

    Route::get('reservasi-saya', 'ReservationController@reservation');
});

Route::get('registrasi', 'RegistrasiController@index');
Route::post('registrasi', 'RegistrasiController@post');
Route::get('setup-account/{username}/verification', 'RegistrasiController@setup');
Route::put('confirmation_email/{id}/verification', 'RegistrasiController@verification');

Auth::routes();
Auth::routes(['register' => false, 'verify' => true]);

Route::get('/admin', function(){
    return redirect('/home');
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::group(['namespace' => 'Admin'], function(){
        Route::get('admin/tambah', 'AdminController@create');
        Route::post('admin/tambah', 'AdminController@store');
        Route::get('admin', 'AdminController@index');
        Route::get('admin/{id}/edit', 'AdminController@edit');
        Route::put('admin/{id}/edit', 'AdminController@update');

        Route::get('admin/perawatan/tambah', 'PerawatanController@create');
        Route::post('admin/perawatan/tambah', 'PerawatanController@store');
        Route::get('admin/perawatan', 'PerawatanController@index');
        Route::get('admin/perawatan/{id}/edit', 'PerawatanController@edit');
        Route::put('admin/perawatan/{id}/edit', 'PerawatanController@update');
        Route::put('admin/perawatan/{id}/ganti-foto', 'PerawatanController@foto');
        Route::delete('admin/perawatan/delete/{id}', 'PerawatanController@destroy');

        Route::get('admin/client', 'ClientController@index');
        Route::get('admin/client/{id}/edit', 'AdminController@editClient');
        Route::put('admin/client/{id}/edit', 'AdminController@updateClient');

        Route::get('admin/baby', 'ClientController@indexBaby');

        Route::get('admin/reservasi', 'ReservasiController@index');
        Route::put('admin/reservasi/setuju/{id}', 'ReservasiController@setuju');
        Route::put('admin/reservasi/tolak/{id}', 'ReservasiController@tolak');
        Route::get('admin/reservasi/excel/download', 'ReservasiController@downloadExcel');
        Route::get('admin/reservasi/pdf/download/{id}', 'ReservasiController@downloadPDF');

        Route::get('admin/profile', 'ProfileController@index');
        Route::put('admin/profile/bio/{id}/edit', 'ProfileController@updateProfile');
        Route::put('admin/profile/avatar/{id}/edit', 'ProfileController@updateAvatar');
    });
});
