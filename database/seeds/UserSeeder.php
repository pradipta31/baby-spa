<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nama' => 'Admin Baby SPA',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('123456'),
            'level' => 1,
            'avatar' => null,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
