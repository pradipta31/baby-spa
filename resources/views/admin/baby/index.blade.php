@extends('admin.layouts.master',['activeMenu' => 'babies'])
@section('title','Bayi')
@section('breadcrumb', 'Data Bayi')
@section('detail_breadcrumb', 'Manajemen Data Bayi')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tableUser" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Umur/Tanggal Lahir</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Tinggi/Berat Badan</th>
                                        <th>Nama Orang Tua</th>
                                        <th>No HP</th>
                                        <th>Foto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($babies as $baby)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$baby->nama_bayi}}</td>
                                            <td>
                                                {{$baby->umur}}/{{date('d M Y', strtotime($baby->tgl_lahir))}}
                                            </td>
                                            <td>
                                                @if ($baby->jenis_kelamin == 'L')
                                                    Laki-laki
                                                @else
                                                    Perempuan
                                                @endif
                                            </td>
                                            <td>
                                                {{$baby->tb_bb}}
                                            </td>
                                            <td>
                                                {{$baby->user->nama}}
                                            </td>
                                            <td>
                                                {{$baby->user->no_hp}}
                                            </td>
                                            <td>
                                                @if ($baby->foto == null)
                                                    Image not found!
                                                @else
                                                    <img src="{{asset('images/baby/'.$baby->foto)}}" alt="" class="img-responsive" style="width: 150px">
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableUser').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }
    </script>
@endsection
