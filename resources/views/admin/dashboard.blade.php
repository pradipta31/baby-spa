@extends('admin.layouts.master',['activeMenu' => 'dashboard'])
@section('title','Dashboard')
@section('breadcrumb', 'Dashboard')
@section('detail_breadcrumb', 'Control Panel')
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-md-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Selamat datang {{Auth::user()->nama}}</h4>
                    Pada halaman dashboard anda dapat melihat beberapa informasi mengenai website anda.
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    
@endsection
