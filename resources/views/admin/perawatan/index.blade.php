@extends('admin.layouts.master',['activeMenu' => 'perawatan'])
@section('title','Data Perawatan')
@section('breadcrumb', 'Data Perawatan')
@section('detail_breadcrumb', 'Manajemen Data Perawatan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <a href="{{url('admin/perawatan/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Data Perawatan
                        </a>
                        <div class="table-responsive">
                            <table id="tablePerawatan" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Perawatan</th>
                                        <th>Foto</th>
                                        <th>Jadwal</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($perawatans as $perawatan)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$perawatan->nama_perawatan}}</td>
                                            <td>
                                                <a href="#" data-toggle="modal" data-target="#showImage{{$perawatan->id}}">
                                                    <img src="{{asset('images/perawatan/'.$perawatan->foto)}}" alt="" class="img-responsive" style="height: 100px; width:100px">
                                                </a>
                                            </td>
                                            <td>
                                                {{$perawatan->jadwal_perawatan}}
                                            </td>
                                            <td>
                                                {!! Str::limit($perawatan->keterangan, 150) !!}
                                                <a href="#" data-toggle="modal" data-target="#showKeterangan{{$perawatan->id}}">Selengkapnya</a>
                                            </td>
                                            <td>
                                                @if ($perawatan->status == 'published')
                                                    <span class="label label-success">Published</span>
                                                @else
                                                    <span class="label label-info">Archived</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{$perawatan->created_at->format('d-m-Y H:i:s')}}
                                            </td>
                                            <td>
                                                <a href="{{url('admin/perawatan/'.$perawatan->id.'/edit')}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-sm btn-danger" onclick="deletePerawatan('{{$perawatan->id}}')">
                                                    <i class="fa fa-trash"></i>
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>
                                        <!-- MODAL FOTO -->
                                        <div class="modal fade" id="showImage{{$perawatan->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <form action="{{url('admin/perawatan/'.$perawatan->id.'/ganti-foto')}}" method="POST" enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="_method" value="put">
                                                        <div class="modal-body">
                                                            <img src="{{asset('images/perawatan/'.$perawatan->foto)}}" alt="" class="img-responsive img-fluid">
                                                            <label for="">Ganti Foto</label>
                                                            <input type="file" name="foto" class="form-control">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END MODAL FOTO -->
                                        <!-- MODAL KETERANGAN -->
                                        <div class="modal bd-example-modal-lg fade" id="showKeterangan{{$perawatan->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            {!! $perawatan->keterangan !!}
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END MODAL KETERANGAN -->
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tablePerawatan').dataTable()
        });

        function fileUrl(){
            $('#showImage').modal('show');
        }

        function keterangan(){
            $('#showKeterangan').modal('show');
        }

        function editUserModal(){
            $('#editUser').modal('show');
        }

        function deletePerawatan(id){
            swal({
                title: "Anda yakin?",
                text: "Data perawatan yang anda pilih akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data perawatan telah dihapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/perawatan/delete')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
