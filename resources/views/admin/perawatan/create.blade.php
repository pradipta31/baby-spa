@section('title','Tambah Perawatan')
@extends('admin.layouts.master',['activeMenu' => 'perawatan'])
@section('title','Tambah Perawatan')
@section('breadcrumb', 'Tambah Perawatan')
@section('detail_breadcrumb', 'Tambah Perawatan Baru')
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('admin/perawatan/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Perawatan Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Nama Perawatan</label>
                                    <input type="text" name="nama_perawatan" class="form-control" value="{{old('nama_perawatan')}}" placeholder="Masukan nama Perawatan">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp</span>
                                        <input type="text" name="harga" class="form-control" style="font-weight: bold" value="{{old('harga')}}" placeholder="Ex: 150000">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Status</label>
                                    <select name="status" class="form-control" value="{{old('status')}}">
                                        <option value="">Pilih Status</option>
                                        <option value="published">Publish</option>
                                        <option value="archived">Archive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Keterangan Perawatan</label>
                                    <textarea name="keterangan" id="" cols="30" rows="10" class="form-control">{{old('keterangan')}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="">Hari</label>
                                            <select name="hari_1" class="form-control" value="{{old('hari_1')}}">
                                                <option value="">Pilih Hari</option>
                                                <option value="Senin">Senin</option>
                                                <option value="Selasa">Selasa</option>
                                                <option value="Rabu">Rabu</option>
                                                <option value="Kamis">Kamis</option>
                                                <option value="Jumat">Jumat</option>
                                                <option value="Sabtu">Sabtu</option>
                                                <option value="Minggu">Minggu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="" style="margin-top: 25px">S/d</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Hari</label>
                                            <select name="hari_2" class="form-control" value="{{old('hari_2')}}">
                                                <option value="">Pilih Hari</option>
                                                <option value="Senin">Senin</option>
                                                <option value="Selasa">Selasa</option>
                                                <option value="Rabu">Rabu</option>
                                                <option value="Kamis">Kamis</option>
                                                <option value="Jumat">Jumat</option>
                                                <option value="Sabtu">Sabtu</option>
                                                <option value="Minggu">Minggu</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Foto</label>
                                    <input type="file" name="foto" class="form-control">
                                    <small>Foto akan ditampilkan di halaman awal jenis perawatan.</small>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" style="margin-left: 20px" onclick="saveBtn(this)">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
<script src="{{asset('backend/plugins/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('backend/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        resize: false,
        plugins: [
            ' advlist anchor autolink codesample fullscreen help',
            ' lists link media noneditable preview',
            ' searchreplace table visualblocks wordcount'
        ],

        toolbar:
        'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist',
    }); 
</script>    
@endsection
