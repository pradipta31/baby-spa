@section('title','Edit Jenis Perawatan')
@extends('admin.layouts.master',['activeMenu' => 'jenis'])
@section('title','Edit Jenis Perawatan')
@section('breadcrumb', 'Edit Jenis Perawatan')
@section('detail_breadcrumb', 'Edit Jenis Perawatan '.$perawatan->nama_perawatan)
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('admin/perawatan/'.$perawatan->id.'/edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Nama Perawatan</label>
                                    <input type="text" name="nama_perawatan" class="form-control" value="{{$perawatan->nama_perawatan}}" placeholder="Masukan Jenis Perawatan">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp</span>
                                        <input type="text" name="harga" class="form-control" style="font-weight: bold" value="{{$perawatan->harga}}" placeholder="Ex: 150000">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Status</label>
                                    <select name="status" class="form-control" value="{{$perawatan->status}}">
                                        <option value="">Pilih Status</option>
                                        <option value="published" {{$perawatan->status == 'published' ? 'selected' : ''}}>Publish</option>
                                        <option value="archived" {{$perawatan->status == 'archived' ? 'selected' : ''}}>Archive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Keterangan Perawatan</label>
                                    <textarea name="keterangan" id="" cols="30" rows="10" class="form-control">{{$perawatan->keterangan}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="">Hari</label>
                                            <select name="hari_1" class="form-control" value="{{$hari_1}}">
                                                <option value="">Pilih Hari</option>
                                                <option value="Senin" {{$hari_1 == 'Senin' ? 'selected' : ''}}>Senin</option>
                                                <option value="Selasa" {{$hari_1 == 'Selasa' ? 'selected' : ''}}>Selasa</option>
                                                <option value="Rabu" {{$hari_1 == 'Rabu' ? 'selected' : ''}}>Rabu</option>
                                                <option value="Kamis" {{$hari_1 == 'Kamis' ? 'selected' : ''}}>Kamis</option>
                                                <option value="Jumat" {{$hari_1 == 'Jumat' ? 'selected' : ''}}>Jumat</option>
                                                <option value="Sabtu" {{$hari_1 == 'Sabtu' ? 'selected' : ''}}>Sabtu</option>
                                                <option value="Minggu" {{$hari_1 == 'Minggu' ? 'selected' : ''}}>Minggu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="" style="margin-top: 25px">S/d</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Hari</label>
                                            <select name="hari_2" class="form-control" value="{{$hari_2}}">
                                                <option value="">Pilih Hari</option>
                                                <option value="Senin" {{$hari_2 == 'Senin' ? 'selected' : ''}}>Senin</option>
                                                <option value="Selasa" {{$hari_2 == 'Selasa' ? 'selected' : ''}}>Selasa</option>
                                                <option value="Rabu" {{$hari_2 == 'Rabu' ? 'selected' : ''}}>Rabu</option>
                                                <option value="Kamis" {{$hari_2 == 'Kamis' ? 'selected' : ''}}>Kamis</option>
                                                <option value="Jumat" {{$hari_2 == 'Jumat' ? 'selected' : ''}}>Jumat</option>
                                                <option value="Sabtu" {{$hari_2 == 'Sabtu' ? 'selected' : ''}}>Sabtu</option>
                                                <option value="Minggu" {{$hari_2 == 'Minggu' ? 'selected' : ''}}>Minggu</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="box-footer">
                            <a href="{{url('admin/perawatan')}}" class="btn btn-default">
                                <i class="fa fa-arrow-left"></i>
                                Kembali
                            </a>
                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
<script src="{{asset('backend/plugins/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('backend/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        resize: false,
        plugins: [
            ' advlist anchor autolink codesample fullscreen help',
            ' lists link media noneditable preview',
            ' searchreplace table visualblocks wordcount'
        ],

        toolbar:
        'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist',
    }); 
</script>    
@endsection
