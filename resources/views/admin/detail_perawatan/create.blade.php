@section('title','Tambah Data Perawatan')
@extends('admin.layouts.master',['activeMenu' => 'jenis'])
@section('title','Tambah Data Perawatan')
@section('breadcrumb', 'Tambah Data Perawatan')
@section('detail_breadcrumb', 'Tambah Data Perawatan Baru')
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('admin/perawatan/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Perawatan Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Jenis Perawatan</label>
                                    <input type="text" name="nama_perawatan" class="form-control" value="{{old('nama_perawatan')}}" placeholder="Masukan Jenis Perawatan">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Foto</label>
                                    <input type="file" name="foto" class="form-control">
                                    <small>Foto akan ditampilkan di halaman awal jenis perawatan.</small>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Keterangan Perawatan</label>
                                    <textarea name="keterangan" id="" cols="30" rows="10" class="form-control">{{old('keterangan')}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Status</label>
                                    <select name="status" class="form-control" value="{{old('status')}}">
                                        <option value="">Pilih Status</option>
                                        <option value="published">Publish</option>
                                        <option value="archived">Archive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
<script src="{{asset('backend/plugins/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('backend/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        resize: false,
        plugins: [
            ' advlist anchor autolink codesample fullscreen help',
            ' lists link media noneditable preview',
            ' searchreplace table visualblocks wordcount'
        ],

        toolbar:
        'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist',
    }); 
</script>    
@endsection
