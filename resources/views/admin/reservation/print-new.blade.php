<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>UMAH BABY SPA</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style type="text/css" media="print">
        @media print{
            @page {size: landscape}}
    </style>
</head>
<body>
		<h4 style="text-decoration: underline">RESERVASI
            <br>
            UMAH BABY SPA
        </h4>

    <br>
    <small>
        <table>
            <tbody>
                <tr>
                    <td>Nama Bayi</td><td> : </td><td>{{$reservasi->baby->nama_bayi}}</td>
                </tr>
                <tr>
                    <td>Nama Orang Tua</td><td> : </td><td>{{$reservasi->baby->user->nama}}</td>
                </tr>
                <tr>
                    <td>Nama Perawatan</td><td> : </td><td>{{$reservasi->perawatan->nama_perawatan}}</td>
                </tr>
                <tr>
                    <td>Tanggal Reservasi</td><td> : </td><td>{{date('d-m-Y', strtotime($reservasi->tgl_reservasi))}}</td>
                </tr>
                <tr>
                    <td>Biaya</td><td> : </td><td>Rp. {{$reservasi->perawatan->harga}}</td>
                </tr>
            </tbody>
        </table>
    </small>
</body>
</html>