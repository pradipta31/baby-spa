@extends('admin.layouts.master',['activeMenu' => 'reservasi'])
@section('title','Reservasi')
@section('breadcrumb', 'Reservasi')
@section('detail_breadcrumb', 'Manajemen Data Reservasi')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tableReservation" class="table table-bordered table-striped">
                                <a href="{{url('admin/reservasi/excel/download')}}" class="btn btn-success" style="margin-bottom: 10px">
                                    <i class="fa fa-download"></i>
                                    Export .xls
                                </a>
                                <thead>
                                    <tr>
                                        <th>Opsi</th>
                                        <th>Nama Bayi</th>
                                        <th>Nama Orang Tua</th>
                                        <th>Nama Perawatan</th>
                                        <th>Tanggal Reservasi</th>
                                        <th>Catatan</th>
                                        <th>Status</th>
                                        <th>Cetak</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($reservations as $res)
                                        <tr>
                                            <td>
                                                @if ($res->status_reservasi == 0)
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                            <span class="">Opsi <i class="caret"></i></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="javascript:void(0);" onclick="setujuReserv('{{$res->id}}')">Setuju</a></li>
                                                            <li class="divider"></li>
                                                            <li><a href="javascript:void(0);" onclick="tolakReserv('{{$res->id}}')">Tolak</a></li>
                                                        </ul>
                                                    </div>
                                                @else
                                                    @if ($res->status_reservasi == 0)
                                                        <span class="label label-info">Dalam Proses</span>
                                                    @elseif($res->status_reservasi == 1)
                                                        <span class="label label-success">Di Setujui</span>
                                                    @elseif($res->status_reservasi == 2)
                                                        <span class="label label-danger">Ditolak</span>
                                                    @endif
                                                @endif
                                            </td>
                                            <td>{{$res->baby->nama_bayi}}</td>
                                            <td>
                                                {{$res->baby->user->nama}}
                                            </td>
                                            <td>{{$res->perawatan->nama_perawatan}}</td>
                                            <td>
                                                <span class="label label-primary">{{date('d-m-Y', strtotime($res->tgl_reservasi))}}</span>
                                            </td>
                                            <td>
                                                @if ($res->catatan == null)
                                                    -
                                                @else
                                                    {{$res->catatan}}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($res->status_reservasi == 0)
                                                    <span class="label label-info">Dalam Proses</span>
                                                @elseif($res->status_reservasi == 1)
                                                    <span class="label label-success">Di Setujui</span>
                                                @elseif($res->status_reservasi == 2)
                                                    <span class="label label-danger">Ditolak</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($res->status_reservasi == 1)
                                                    <a href="{{url('admin/reservasi/pdf/download/'.$res->id)}}" class="btn btn-primary btn-sm" target="_blank">
                                                        <i class="fa fa-file-pdf-o"></i>
                                                        Download
                                                    </a>
                                                @else
                                                -
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formSetuju">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
    </form>
    <form class="hidden" action="" method="post" id="formTolak">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableReservation').dataTable()
        });

        function setujuReserv(id){
            swal({
                title: "Anda yakin?",
                text: "Reservasi akan disetujui. Client akan menerima konfirmasi reservasi melalui email!",
                icon: "warning",
                buttons: true,
                dangerMode: false
            })
            .then((willActive) => {
                if (willActive) {
                    swal("Berhasil! Reservasi berhasil disetujui!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formSetuju').attr('action', '{{url('admin/reservasi/setuju')}}/'+id);
                        $('#formSetuju').submit();
                    }); 
                }
            });
        }

        function tolakReserv(id){
            swal({
                title: "Anda yakin?",
                text: "Reservasi akan ditolak. Client akan menerima konfirmasi penolakan melalui email!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willActive) => {
                if (willActive) {
                    swal("Reservasi client telah ditolak!", {
                        icon: "error",
                    }).then((res) => {
                        $('#formTolak').attr('action', '{{url('admin/reservasi/tolak')}}/'+id);
                        $('#formTolak').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
