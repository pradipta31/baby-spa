@extends('admin.layouts.master',['activeMenu' => 'client'])
@section('title','Client')
@section('breadcrumb', 'Client')
@section('detail_breadcrumb', 'Manajemen Data Client')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('admin.layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tableUser" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Level</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($clients as $user)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$user->nama}}</td>
                                            <td>
                                                {{$user->username}}
                                            </td>
                                            <td>{{$user->email}}</td>
                                            <td>
                                                <span class="label label-primary">Client</span>
                                            </td>
                                            <td>
                                                @if ($user->status == 1)
                                                    <span class="label label-success">Aktif</span>
                                                @else
                                                    <span class="label label-warning">Non Aktif</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{url('admin/client/'.$user->id.'/edit')}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableUser').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }
    </script>
@endsection
