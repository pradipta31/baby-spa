<li class="header">USER NAVIGATION</li>
<li class="treeview {{$activeMenu == 'admin' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-users"></i>
        <span>Admin</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('admin/tambah')}}"><i class="fa fa-plus"></i> Tambah Admin Baru</a></li>
        <li><a href="{{url('admin')}}"><i class="fa fa-circle-o"></i> Data Admin</a></li>
    </ul>
</li>
{{-- <li class="{{$activeMenu == 'client' ? 'active' : ''}}"><a href="{{url('admin/client')}}"><i class="fa fa-users"></i> <span>Data Client</span></a></li> --}}
<li class="{{$activeMenu == 'babies' ? 'active' : ''}}"><a href="{{url('admin/baby')}}"><i class="fa fa-users"></i> <span>Data Bayi</span></a></li>

<li class="header">MASTER NAVIGATION</li>
<li class="treeview {{$activeMenu == 'perawatan' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-medkit"></i>
        <span>Data Perawatan</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('admin/perawatan/tambah')}}"><i class="fa fa-plus"></i> Tambah Perawatan</a></li>
        <li><a href="{{url('admin/perawatan')}}"><i class="fa fa-circle-o"></i> Data Perawatan</a></li>
    </ul>
</li>
<li class="{{$activeMenu == 'reservasi' ? 'active' : ''}}"><a href="{{url('admin/reservasi')}}"><i class="fa fa-cart-arrow-down"></i> <span>Data Reservasi</span></a></li>