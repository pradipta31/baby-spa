<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                @if (Auth::user()->ava == null)
                    <img src="{{asset('images/avatar.jpeg')}}" class="img-circle" alt="User Image">
                @else
                    <img src="{{asset('images/ava/'.Auth::user()->ava)}}" class="img-circle" alt="User Image">
                @endif
            </div>
            <div class="pull-left info">
                <p>{{\Str::limit(Auth::user()->nama, 24)}}</p>
                <a><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">DASHBOARD NAVIGATION</li>
            <li class="{{$activeMenu == 'dashboard' ? 'active' : ''}}"><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            @include('admin.layouts.nav-admin')
        </ul>
    </section>
</aside>
  