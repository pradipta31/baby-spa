<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Baby SPA | Verification Email</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{asset('backend/bower_components/Ionicons/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('backend/dist/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('backend/plugins/iCheck/square/blue.css')}}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Baby</b> SPA
  </div>
  
  <div class="login-box-body text-center">
    <p class="login-box-msg">
        <img src="{{asset('logo/icon-check.png')}}" alt="" style="img-responsive" height="130px" width="100px">
        <h3 style="margin-top: -20px">Selamat!</h3>
        Email anda berhasil di verifikasi!
        <br>
        Sekarang anda sudah dapat melakukan login ke dalam sistem dengan mengklik tombol dibawah ini.
    </p>
    <form action="{{url('confirmation_email/'.$user->id.'/verification')}}" method="POST">
        {{csrf_field()}}
      <input type="hidden" name="_method" value="put">
      <button type="submit" class="btn btn-primary btn-block btn-sm">Verifikasi Akun anda</button>
    </form>
  </div>
</div>
<script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('backend/plugins/iCheck/icheck.min.js')}}"></script>
</body>
</html>
