{{-- <footer class="wpm_footer">
    <div class="container">
     <div class="row">
         <div class="col-sm-4">
             <h2>ABOUT MULTIMAG</h2>
             <p>Lorem ipsum dolor sit amet, inimicus at et viets, illum partem diceret. Meis suscipiantur mel cu. Debitis prodesset definiebas.</p>		
             <ul class="wpm_list">
                 <li> <i class="fa fa-location-arrow fa-fw"></i> 122 Baker Street, Marylebone London, W1U 6TX</li>
                 <li> <i class="fa fa-phone fa-fw"></i> +8801354702974</li>
                 <li> <i class="fa fa-envelope fa-fw"></i><a href=""> info@Html5Load.com</li>
             </ul>
             <h2 class="stay_text">STAY CONNECTED</h2>
                 <a class="social_icon" href="#"><i class="fa fa-facebook"></i></a>
                 <a class="social_icon" href="#"><i class="fa fa-google-plus"></i></a>
                 <a class="social_icon" href="#"><i class="fa fa-twitter"></i></a>
                 <a class="social_icon" href="#"><i class="fa fa-youtube"></i></a>
         </div>
         <div class="col-sm-4">
             <h2>FEATURED ARTICLES</h2>
             <div class="footer_some_title">
                 <img src="{{asset('frontend/images/work/01.jpg')}}" alt="">
                 <h4><a href="#">Meet Tiffany's First Ever Female Design Director</a></h4>
                    <a href="#" class="">July 4, 2014 03:57 pm</a>
             </div>
             <div class="footer_some_title">
                 <img src="{{asset('frontend/images/work/02.jpg')}}" alt="">
                 <h4><a href="#">Meet Tiffany's First Ever Female Design Director</a></h4>
                    <a href="#" class="">July 4, 2014 03:57 pm</a>
             </div>
             <div class="footer_some_title">
                 <img src="{{asset('frontend/images/work/03.jpg')}}" alt="">
                 <h4><a href="#">Meet Tiffany's First Ever Female Design Director</a></h4>
                    <a href="#" class="">July 4, 2014 03:57 pm</a>
             </div>
             <div class="footer_some_title">
                 <img src="{{asset('frontend/images/work/04.jpg')}}" alt="">
                 <h4><a href="#">Meet Tiffany's First Ever Female Design Director</a></h4>
                    <a href="#" class="">July 4, 2014 03:57 pm</a>
             </div>
         </div>
         <div class="col-sm-4">
             <h2>INSTAGRAM</h2>
             <div class="footer_img_grup">
                 <img src="{{asset('frontend/images/work/01.jpg')}}" alt="">
                 <img src="{{asset('frontend/images/work/02.jpg')}}" alt="">
                 <img src="{{asset('frontend/images/work/03.jpg')}}" alt="">
                 <img src="{{asset('frontend/images/work/04.jpg')}}" alt="">
                 <img src="{{asset('frontend/images/work/05.jpg')}}" alt="">
                 <img src="{{asset('frontend/images/work/06.jpg')}}" alt="">
             </div>
         </div>
     </div>
 </div>
</footer> --}}

<section class="wpm_frooter_ending">
    <div class="container">
        <div class="col-sm-12 wpm_mobile_center">
            <p>&copy;{{date('Y')}} Sistem Informasi Reservasi Baby SPA</p>
        </div>
    </div>
</section>
<section class="wpm_frooter_ending" style="display: none">
 <div class="container">
     <div class="col-sm-12 wpm_mobile_center">
             <p>&copy;{{date('Y')}} Sistem Informasi Reservasi Baby SPA</p>
             <div class="copytext" id="button1" style="color: black; ">Design By <a href="http://html5load.com/" target="_blank" class="copylink">html5load.Com</a></div>
      </div>  
  </div>   
</section>  