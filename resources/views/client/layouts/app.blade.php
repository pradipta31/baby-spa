<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Baby SPA | @yield('title')</title>
        @toastr_css
        <!-- Bootstrap -->
        <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('frontend/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/responsive.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style_common.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style6.css')}}" />	
        <!-- IMPORT FROM ADMIN -->
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
        <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

        <script type="text/javascript" src="{{asset('frontend/js/TweenMax.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/DrawSVGPlugin.min.js')}}"></script>
        @yield('frontcss')
    </head>
    
    <body>
    @include('client.layouts.navigation', ['activeMenu' => $activeMenu])

    @yield('contentfront')

    @include('client.layouts.footer')
    @jquery
    @toastr_js
    @toastr_render
    <script src="{{asset('frontend/js/jquery.min.js')}}"></script>
    <script src="{{asset('frontend/js/Main-script.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script type="text/javascript">
        function saveBtn(res){
            res.innerHTML = '<i class="fa fa-spinner fa-spin"></i> Sedang di proses';
        }
        document.ready(function(){
            $('#button1').hide();
        });
    </script>
    @yield('frontjs')
   
   </body>
   </html>