<header class="wpm_header">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <a class="navbar-brand" href="{{url('/')}}"> <img class="wpm_logo" src="{{asset('images/logo_home.jpeg')}}" alt=""></a>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-8">
                <nav class="navbar navbar-default wpm_navber">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right wpm_menu"> 
                            @if (Auth::check())
                                <li class="{{$activeMenu == 'home' ? 'active' : ''}}"><a href="{{url('/')}}">Home</a></li>
                                <li class="{{$activeMenu == 'perawatan' ? 'active' : ''}}"><a href="{{url('perawatan')}}">Perawatan</a></li>
                                <li class="{{$activeMenu == 'profile' ? 'active' : ''}}"><a href="{{url('myprofile')}}">Profile Saya</a> </li>
                                <li class="{{$activeMenu == 'reservation' ? 'active' : ''}}"><a href="{{url('reservasi-saya')}}">Reservasi Saya</a> </li>
                                <li role="presentation" class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                                       {{Auth::user()->nama}} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="background: white; color: #333">Sign Out <span style="margin-left: 50%"><i class="fa fa-sign-out"></i></span></a> </li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </ul>
                                </li>
                                
                            @else
                                <li class="{{$activeMenu == 'home' ? 'active' : ''}}"><a href="{{url('/')}}">Home</a></li>
                                <li class="{{$activeMenu == 'perawatan' ? 'active' : ''}}"><a href="{{url('perawatan')}}">Perawatan</a></li>
                                <li><a href="{{url('login')}}">Sign in</a> </li>
                            @endif
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>