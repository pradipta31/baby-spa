@extends('client.layouts.app',['activeMenu' => 'perawatan'])
@section('title', 'Perawatan '.$perawatan->nama_perawatan)
@section('contentfront')

<section class="wpm_title_ber">
    <div class="wpm_opacity_bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Perawatan</h2>
                    <div class="ber_link text-center">
                        <h5 class="sub_title">{{$perawatan->nama_perawatan}}</h5>
                        <p> <i class="fa fa-angle-double-right"></i> <a href="{{url('/')}}">Home</a> / {{$perawatan->nama_perawatan}} <i class="fa fa-angle-double-left"></i> </p>
                    </div>
                </div>
            </div>
        </div> 
    </div>    
</section>
 
 
<section class="wpm_blogarticle_area">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <img src="{{asset('images/perawatan/'.$perawatan->foto)}}" alt="">
                <div class="blog_link_area">
                    <h3>
                        <a href="#">{{$perawatan->nama_perawatan}}</a>
                        
                    </h3>
                    <a href="#" class="date"><i class="fa fa-money"></i> Rp. {{number_format($perawatan->harga,2,",",".")}}</a>
                    <a href="#" class="date"><i class="fa fa-clock-o"></i> {{$perawatan->jadwal_perawatan}}</a>
                </div>
                <p> {!! $perawatan->keterangan !!}</p>
                @if (Auth::check())
                    <a href="{{url('perawatan/book-now/'.$perawatan->id)}}" class="btn btn-primary btn-block btn-md"><i class="fa fa-cart-plus"></i> Book Now!</a>
                @else
                    <a href="{{url('login')}}" class="btn btn-primary btn-block btn-md"><i class="fa fa-cart-plus"></i> Book Now!</a>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection