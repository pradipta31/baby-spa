@extends('client.layouts.app',['activeMenu' => 'perawatan'])
@section('title', 'Home')
@section('contentfront')
    <section class="wpm_title_ber">
        <div class="wpm_opacity_bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>Services</h2>
                        <div class="ber_link text-center">
                            <h5 class="sub_title">WE HAVE 25 YEARS EXERIENCE IN BABY CARE</h5>
                            <p> <i class="fa fa-angle-double-right"></i> <a href="{{url('/')}}">Home</a>  <i class="fa fa-angle-double-left"></i> </p>
                        </div>
                    </div>
                </div>
            </div> 
        </div>    
    </section>


    <section class="our_service_area">
    	<div class="container">
			<div class="row">
            	<h2>Our <span class="wpm_color_word">Services</span></h2>
                <div class="wpm_border"> <i class="fa fa-smile-o"></i> </div>
            	<div class="row">
                    @foreach ($perawatans as $perawatan)
                        <div class="col-sm-4" style="float: right">
                            <div class="wpm_around_img_inner">
                                <a href="{{url('perawatan/'.$perawatan->url)}}"><img class="box_img" src="{{asset('images/perawatan/'.$perawatan->foto)}}" style="width: 350px; height: 200px" alt=""></a>
                                <div class="clearfix"></div>
                            </div>
                            <a href="{{url('perawatan/'.$perawatan->url)}}"><h4>{{$perawatan->nama_perawatan}}</h4> </a>
                            <p>{!! Str::limit($perawatan->keterangan, 100) !!}</p>
                            <a href="{{url('perawatan/'.$perawatan->url)}}" class="btn btn-info"><i class="fa fa-arrow-right"></i> Read More</a>
                            <hr>
                        </div>
                    @endforeach
                </div>
            </div>        
        </div>
        <div class="text-center">
            {!! $perawatans->links() !!}
        </div>
    </section>
@endsection