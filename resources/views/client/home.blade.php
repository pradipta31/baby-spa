@extends('client.layouts.app',['activeMenu' => 'home'])
@section('title', 'Home')
@section('contentfront')
    <section class="wpm_slider_area">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
                
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">            
                <div class="item active">
                    <img src="{{asset('frontend/images/slider/01.jpg')}}" alt="...">
                    <div class="carousel-caption">
                        <h1>Umah Baby SPA </h1><br>
                    </div>
                </div>
                
                <div class="item">
                    <img src="{{asset('frontend/images/slider/02.jpg')}}" alt="...">
                    <div class="carousel-caption">
                        <h1>Umah Baby SPA</h1><br>
                    </div>
                </div>
                <div class="item">
                    <img src="{{asset('frontend/images/slider/03.jpg')}}" alt="...">
                    <div class="carousel-caption">
                        <h1>Umah Baby SPA</h1><br>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <section class="wpm_featured_area">
        <div class="container">
            <div class="row">
                <h2>RESERVASI<span class="wpm_color_word"> UMAH BABY SPA</span></h2>
                <div class="wpm_border"> <i class="fa fa-smile-o"></i> </div>
                <h6 class="sub_title">Perum Uma Sari II No. 1 Br. Pengilingan - Dalung, Kuta Utara, Badung, Bali</h6>
                <div class="col-sm-4">
                    <img src="{{asset('frontend/images/baby.jpg')}}" alt="">
                </div>
                <div class="col-sm-8">
                    <div class="featured_text">
                        <h5>MANFAAT 1 Mengoptimalkan Perkembangan Motorik</h5>
                        <div class="wpm_roted_box"><i class="fa fa-futbol-o"></i> </div> 
                        <p>Pada usia 0-1 tahun adalah fase belajar bagi si kecil, baik belajar telungkup, merangkak, duduk, dan berdiri. Dengan metode berenang pada Umah Baby SPA diharapkan si kecil dapat lebih optimal dalam perkembangan motorik si kecil</p>
                    </div>  
                    <div class="featured_text">
                        <h5>MANFAAT 2 Menstimulasi Otot Menjadi Lebih Kuat</h5>
                        <div class="wpm_roted_box"> <i class="fa fa-star-o"></i> </div>
                        <p>Baby SPA dengan hidroterapi dapat menstimulasi otot lengan dan kaki si kecil menjadi lebih kuat. Hidroterapi terbukti dapat belajar berjalan lebih cepat karena otot kaki si kecil lebih terlatih dengan terapi</p>
                    </div>  
                    <div class="featured_text">
                        <h5>MANFAAT 3 Si Kecil menjadi lebih Rileks</h5>
                        <div class="wpm_roted_box"> <i class="fa fa-sun-o"></i> </div>
                        <p>Terapi air membuat si kecil menjadi lebih rileks, sehingga meningkatkan nafsu makan dan pola tidur yang lebih baik</p>
                    </div>  
                </div>
            </div>
        </div>
    </section>


    <section class="wpm_email_area">
        <div class="wpm_opacity_bg">
            <div class="container">
                <div class="row">
                    <h2>Hello <span class="wpm_color_word">World</span></h2>
                    <div class="wpm_border"> </div>
                    <h3 class="sub_title" style="font-weight: normal; line-height: 1.5; font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; font-size: 1.5em">
                        -------------------------Always kiss your children every night, even if they are already sleep-------------------------
                    </h3>
                </div>
            </div>
        </div>  
    </section>


    <section class="our_service_area">
    	<div class="container">
			<div class="row">
            	<h2>Our <span class="wpm_color_word">Services</span></h2>
                <div class="wpm_border"> <i class="fa fa-smile-o"></i> </div>
            	@foreach ($perawatans as $perawatan)
                    <div class="col-sm-4">
                        <div class="wpm_around_img_inner">
                            <a href="{{url('perawatan/'.$perawatan->url)}}"><img class="box_img" src="{{asset('images/perawatan/'.$perawatan->foto)}}" alt=""></a>
                            <div class="clearfix"></div>
                        </div>
                        <a href="{{url('perawatan/'.$perawatan->url)}}"><h4>{{$perawatan->nama_perawatan}}</h4> </a>
                        <p>{!! Str::limit($perawatan->keterangan, 200) !!}</p>
                        <a href="{{url('perawatan/'.$perawatan->url)}}" class="btn btn-info btn-md"><i class="fa fa-arrow-right"></i> Read More</a>
                    </div>
                @endforeach
            </div>        
        </div>
        <style>
            .tombol {
              border: 2px solid black;
              background-color: white;
              color: black;
              padding: 10px 20px;
              font-size: 16px;
              cursor: pointer;
            }
            
            /* Green */
            .success {
              border-color: #4CAF50;
              color: green;
            }
            
            .success:hover {
              background-color: #4CAF50;
              color: white;
            }
            
            /* Blue */
            .info {
              border-color: #6cbbfc;
              color: rgb(96, 176, 255);
            }
            
            .info:hover {
              background: #54a7eb;
              color: white;
            }
            
            /* Orange */
            .warning {
              border-color: #ff9800;
              color: orange;
            }
            
            .warning:hover {
              background: #ff9800;
              color: white;
            }
            
            /* Red */
            .danger {
              border-color: #f44336;
              color: red;
            }
            
            .danger:hover {
              background: #f44336;
              color: white;
            }
            
            /* Gray */
            .default {
              border-color: #e7e7e7;
              color: black;
            }
            
            .default:hover {
              background: #e7e7e7;
            }
            </style>
        <div class="text-center">
            <a href="{{url('perawatan')}}" class="btn btn-lg tombol info"><i class="fa fa-arrow-right"></i> Tampilkan Lebih Banyak</a>
        </div>
    </section>
    <hr class="call_us_area" style="border-bottom: 0px">
@endsection