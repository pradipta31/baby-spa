@extends('client.layouts.app',['activeMenu' => 'perawatan'])
@section('title', 'Home')
@section('frontcss')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/set2.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/normalize.css')}}" />
@endsection
@section('contentfront')
<section class="wpm_title_ber">
    <div class="wpm_opacity_bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Reservasi</h2>
                    <div class="ber_link text-center">
                        <h5 class="sub_title">Reservasi</h5>
                        <p> <i class="fa fa-angle-double-right"></i> <a href="{{url('/')}}">Home</a> /  {{$perawatan->nama_perawatan}}<i class="fa fa-angle-double-left"></i> </p>
                    </div>
                </div>
            </div>
        </div> 
    </div>    
</section>

<section class="wpm_contact_area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="wpm_address">
                    <h3>{{$perawatan->nama_perawatan}}</h3>
                    <div class="row">
                        <div class="col-sm-4">
                            <p>Nama Perawatan</p>
                            <p>Jadwal Perawatan</p>
                        </div>
                        <div class="col-sm-8">
                            <p>: {{$perawatan->nama_perawatan}}</p>
                            <p>: {{$perawatan->jadwal_perawatan}}</p>
                        </div>
                        <div class="col-sm-12">
                            <hr>
                            {!! $perawatan->keterangan !!}
                            <hr>
                            <p style="color: #4CAF50; font-size: 2.5rem; font-weight: bold">Rp. {{number_format($perawatan->harga, 2,',','.')}}</p>
                        </div>
                    </div>
                </div>
                <div class="embed-responsive embed-responsive-16by9 wpm_maph ">
                    <img src="{{asset('images/perawatan/'.$perawatan->foto)}}" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Booking Form</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{url('perawatan/book-now')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="perawatan_id" value="{{$perawatan->id}}">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Nama Bayi</label>
                                    <div class="col-sm-10">
                                        <select name="baby_id" value="{{old('baby_id')}}" class="form-control">
                                            @foreach ($babies as $baby)
                                                <option value="{{$baby->id}}">{{$baby->nama_bayi}}</option>
                                            @endforeach
                                        </select>
                                        <small>Tambahkan data buah hati anda <a href="{{url('myprofile/new-baby/'.$perawatan->id)}}">disini</a></small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Tanggal Pemesanan</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="tgl_reservasi" value="{{old('tgl_reservasi')}}" id="datepicker" class="form-control" style="padding: 0 10px">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Catatan</label>
                                    <div class="col-sm-10">
                                        <textarea name="catatan" class="form-control" id="" cols="30" rows="3">{{old('catatan')}}</textarea>
                                        <small>NB: Catatan dapat dikosongkan.</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">        
                                    <div class="col-sm-offset-2 col-sm-10">
                                      <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('frontjs')
    <script type="text/javascript">
        // var data = {!! json_encode($perawatan) !!}
        // console.log(data['jadwal_perawatan']);
        var dateToday = new Date();
        
        $('#datepicker').datepicker({
            autoclose: true,
            startDate: dateToday,
            // daysOfWeekDisabled: [0,1]
        }).datepicker("setDate", new Date());
        // console.log(dateMin);
    </script>
@endsection