@extends('client.layouts.app',['activeMenu' => 'perawatan'])
@section('title', 'Home')
@section('frontcss')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/set2.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/normalize.css')}}" />
@endsection
@section('contentfront')
<section class="wpm_title_ber">
    <div class="wpm_opacity_bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Reservasi</h2>
                    <div class="ber_link text-center">
                        <h5 class="sub_title">Reservasi</h5>
                        <p> <i class="fa fa-angle-double-right"></i> <a href="{{url('/')}}">Home</a> /  Pemesanan berhasil<i class="fa fa-angle-double-left"></i> </p>
                    </div>
                </div>
            </div>
        </div> 
    </div>    
</section>

<section class="wpm_contact_area">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-check"></i> Reservation Success!</div>
                    <div class="panel-body">
                        Pemesanan anda berhasil di proses!
                        <br>
                        Pastikan email yang anda gunakan masih aktif, karena kami akan mengirim booking code anda melalui email!
                        <br>
                        Cek status reservasi anda <a href="{{url('reservasi-saya')}}">disini</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection