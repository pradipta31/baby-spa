@extends('client.layouts.app',['activeMenu' => 'reservation'])
@section('title', 'Home')
@section('frontcss')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/set2.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/normalize.css')}}" />
@endsection
@section('contentfront')
<section class="wpm_title_ber">
    <div class="wpm_opacity_bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Reservasi Saya</h2>
                    <div class="ber_link text-center">
                        <h5 class="sub_title">Reservasi Saya</h5>
                        <p> <i class="fa fa-angle-double-right"></i> <a href="{{url('/')}}">Home</a> /  Reservasi Saya<i class="fa fa-angle-double-left"></i> </p>
                    </div>
                </div>
            </div>
        </div> 
    </div>    
</section>

<section class="wpm_contact_area">
    <div class="container">
        <div class="row">
            @foreach ($reservations as $res)
            <div class="col-sm-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-cart-arrow-down"></i> <a href="{{url('perawatan/'.$res->perawatan->url)}}">{{$res->perawatan->nama_perawatan}} </a>
                        @if ($res->status_reservasi == 0)
                            <span class="label label-warning">Dalam Proses</span>
                        @elseif($res->status_reservasi == 1)
                            <span class="label label-success">Disetujui</span>
                        @elseif($res->status_reservasi == 2)
                            <span class="label label-danger">Ditolak</span>
                        @endif
                    </div>
                    <div class="panel-body">
                        <h4>{{$res->nama_bayi}}</h4>
                        <hr>
                        @if ($res->catatan == null)
                            <h5>Tidak ada catatan.</h5>
                        @else
                            <h5>{{$res->catatan}}</h5>
                        @endif
                    </div>
                    <div class="panel-footer text-muted">
                        Tanggal Reservasi: {{date('d M Y', strtotime($res->tgl_reservasi))}}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection