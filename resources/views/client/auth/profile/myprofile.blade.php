@extends('client.layouts.app',['activeMenu' => 'profile'])
@section('title', 'Home')
@section('frontcss')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/set2.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/normalize.css')}}" />
@endsection
@section('contentfront')
<section class="wpm_title_ber">
    <div class="wpm_opacity_bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Profile Saya</h2>
                    <div class="ber_link text-center">
                        <h5 class="sub_title">Profile Saya</h5>
                        <p> <i class="fa fa-angle-double-right"></i> <a href="{{url('/')}}">Home</a> / {{$user->nama}} <i class="fa fa-angle-double-left"></i> </p>
                    </div>
                </div>
            </div>
        </div> 
    </div>    
</section>

<section class="wpm_contact_area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">My Profile</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Nama</label>
                                <div class="col-sm-9">
                                    <span> : {{$user->nama}}</span>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Username</label>
                                <div class="col-sm-9">
                                    <span> : {{$user->username}}</span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Email</label>
                                <div class="col-sm-9">
                                    <span> : {{$user->email}}</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">No Telp</label>
                                <div class="col-sm-9">
                                    <span> : {{$user->no_hp}}</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Alamat</label>
                                <div class="col-sm-9">
                                    <span> : {{$user->alamat}}</span>
                                </div>
                            </div>
                            <div class="col-sm-12"><hr></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                @if ($user->avatar == null)
                                    <img src="{{asset('images/ava/avatar.png')}}" alt="" class="img-responsive img-fluid" style="height: 150px; width: 125px">
                                @else
                                    <img src="{{asset('images/ava/'.$user->avatar)}}" alt="" class="img-responsive img-fluid" style="height: 150px; width: 125px">
                                @endif
                                <br>
                                <a href="{{url('myprofile/edit')}}">Edit Profile</a>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Data Bayi</div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach ($babies as $baby)
                            <h4 style="margin-left: 5px">{{$no++}}. {{$baby->nama_bayi}}</h4>
                            <br>
                                <div class="col-sm-4">
                                    <p>Umur/Tanggal Lahir </p>
                                    <p>Jenis Kelamin </p>
                                    <p>Tinggi/Berat Badan</p>
                                </div>
                                <div class="col-sm-8">
                                    <p>: {{$baby->umur}} / {{date('d M Y', strtotime($baby->tgl_lahir))}}</p>
                                    <p>: 
                                        @if ($baby->jenis_kelamin == 'L')
                                            Laki-laki
                                        @else
                                            Perempuan
                                        @endif
                                    </p>
                                    <p>: {{$baby->tb_bb}}</p>
                                </div>
                                <div class="col-sm-12">
                                    <a href="{{url('myprofile/baby/'.$baby->id.'/edit')}}">Edit Data</a>
                                    <hr style="background: black">
                                </div>
                                
                            @endforeach
                            <div class="col-sm-12">
                                <a href="{{url('myprofile/new-baby')}}">+ Data Bayi Baru</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection