@extends('client.layouts.app',['activeMenu' => 'profile'])
@section('title', 'Home')
@section('frontcss')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/set2.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/normalize.css')}}" />
@endsection
@section('contentfront')
<section class="wpm_title_ber">
    <div class="wpm_opacity_bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Tambah Data Bayi</h2>
                    <div class="ber_link text-center">
                        <h5 class="sub_title">Tambah Data Bayi</h5>
                        <p> <i class="fa fa-angle-double-right"></i> <a href="{{url('/')}}">Home</a> /  Data Bayi Baru<i class="fa fa-angle-double-left"></i> </p>
                    </div>
                </div>
            </div>
        </div> 
    </div>    
</section>

<section class="wpm_contact_area">
    <div class="container">
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">+ Tambah Data Bayi Baru</div>
                <div class="panel-body">
                    <form class="form-horizontal" action="{{url('myprofile/new-baby')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_user" value="{{Auth::user()->id}}">
                        @if ($perawatan == NULL)
                            
                        @else
                            <input type="hidden" name="id_perawatan" value="{{$perawatan}}">
                        @endif
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Nama Bayi</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nama_bayi" class="form-control" value="{{old('nama_bayi')}}" placeholder="Masukan Nama Bayi">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">Umur / Tanggal Lahir</label>
                                <div class="col-sm-8">
                                    <input type="text" name="umur" class="form-control" value="{{old('umur')}}" placeholder="Ex: 6 bulan">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label col-sm-1">/</label>
                                <div class="col-sm-11">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="tgl_lahir" value="{{old('tgl_lahir')}}" class="form-control pull-right" id="datepicker">
                                    </div>
                                    {{-- <input type="date" name="tgl_lahir" value="{{old('tgl_lahir')}}" id="datepicker"> --}}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label col-sm-6">Tinggi / Berat Badan</label>
                                <div class="col-sm-6">
                                    <input type="text" name="tinggi_badan" class="form-control" value="{{old('tinggi_badan')}}" placeholder="Ex: 70cm">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label col-sm-1">/</label>
                                <div class="col-sm-6">
                                    <input type="text" name="berat_badan" class="form-control" value="{{old('berat_badan')}}" placeholder="Ex: 5kg">
                                </div>
                                <label class="control-label col-sm-5">Jenis Kelamin</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <select name="jenis_kelamin" value="{{old('jenis_kelamin')}}" class="form-control">
                                        <option value="">Pilih Jenis Kelamin</option>
                                        <option value="L">Laki-Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Foto</label>
                                <div class="col-sm-10">
                                    <input type="file" name="foto" class="form-control">
                                    <small>Foto dapat dikosongkan jika tidak ada.</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">        
                                <div class="col-sm-offset-2 col-sm-10">
                                  <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('frontjs')
    <script type="text/javascript">
        $('#datepicker').datepicker({
            autoclose: true
        })
    </script>
@endsection