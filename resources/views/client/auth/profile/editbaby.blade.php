@extends('client.layouts.app',['activeMenu' => 'profile'])
@section('title', 'Home')
@section('frontcss')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/set2.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/normalize.css')}}" />
@endsection
@section('contentfront')
<section class="wpm_title_ber">
    <div class="wpm_opacity_bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Edit Baby</h2>
                    <div class="ber_link text-center">
                        <h5 class="sub_title">Edit Baby</h5>
                        <p> <i class="fa fa-angle-double-right"></i> <a href="{{url('/')}}">Home</a> / Edit Baby {{$baby->nama_bayi}}<i class="fa fa-angle-double-left"></i> </p>
                    </div>
                </div>
            </div>
        </div> 
    </div>    
</section>

<section class="wpm_contact_area">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">{{$baby->nama_bayi}}</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Nama</label>
                                <div class="col-sm-9">
                                    <span> : {{$baby->nama_bayi}}</span>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Umur</label>
                                <div class="col-sm-9">
                                    <span> : {{$baby->umur}}</span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Tanggal Lahir</label>
                                <div class="col-sm-9">
                                    <span> : {{date('d M Y',strtotime($baby->tgl_lahir))}}</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">No Telp</label>
                                <div class="col-sm-9">
                                    <span> : 
                                        @if ($baby->jenis_kelamin == 'L')
                                            Laki-laki
                                        @else
                                            Perempuan
                                        @endif
                                    </span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Tinggi/Berat Badan</label>
                                <div class="col-sm-9">
                                    <span> : {{$baby->tb_bb}}</span>
                                </div>
                            </div>
                            <div class="col-sm-12"><hr></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                @if ($baby->foto == null)
                                    Image not found!
                                @else
                                    <img src="{{asset('images/baby/'.$baby->foto)}}" alt="" class="img-responsive img-fluid">
                                @endif
                                <br>
                                <a href="{{url('myprofile')}}"><< Halaman Profile</a>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Edit Data</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{url('myprofile/baby/'.$baby->id.'/edit')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="_method" value="put">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Nama Bayi</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="nama_bayi" class="form-control" value="{{$baby->nama_bayi}}" placeholder="Masukan Nama Bayi">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Umur / Tanggal Lahir</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="umur" class="form-control" value="{{$baby->umur}}" placeholder="Ex: 6 bulan">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-1">/</label>
                                    <div class="col-sm-11">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="tgl_lahir" value="{{$tgl_lahir}}" class="form-control pull-right" id="datepicker">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-6">Tinggi / Berat Badan</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="tinggi_badan" class="form-control" value="{{$tb}}" placeholder="Ex: 70cm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-1">/</label>
                                    <div class="col-sm-5">
                                        <input type="text" name="berat_badan" class="form-control" value="{{$bb}}" placeholder="Ex: 5kg">
                                    </div>
                                    <label class="control-label col-sm-5">Jenis Kelamin</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <select name="jenis_kelamin" value="{{$baby->jenis_kelamin}}" class="form-control">
                                            <option value="">Pilih Jenis Kelamin</option>
                                            <option value="L" {{$baby->jenis_kelamin == 'L' ? 'selected' : ''}}>Laki-Laki</option>
                                            <option value="P" {{$baby->jenis_kelamin == 'P' ? 'selected' : ''}}>Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Foto</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="foto" class="form-control">
                                        <small>Foto dapat dikosongkan jika tidak ada.</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">        
                                    <div class="col-sm-offset-2 col-sm-10">
                                      <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('frontjs')
    <script type="text/javascript">
        $('#datepicker').datepicker({
            autoclose: true
        })
    </script>
@endsection