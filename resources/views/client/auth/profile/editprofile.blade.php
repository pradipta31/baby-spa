@extends('client.layouts.app',['activeMenu' => 'profile'])
@section('title', 'Home')
@section('frontcss')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/set2.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/normalize.css')}}" />
@endsection
@section('contentfront')
<section class="wpm_title_ber">
    <div class="wpm_opacity_bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Edit Profile</h2>
                    <div class="ber_link text-center">
                        <h5 class="sub_title">Edit Profile</h5>
                        <p> <i class="fa fa-angle-double-right"></i> <a href="{{url('/')}}">Home</a> / Edit Profile<i class="fa fa-angle-double-left"></i> </p>
                    </div>
                </div>
            </div>
        </div> 
    </div>    
</section>

<section class="wpm_contact_area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">My Profile</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Nama</label>
                                <div class="col-sm-9">
                                    <span> : {{$user->nama}}</span>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Username</label>
                                <div class="col-sm-9">
                                    <span> : {{$user->username}}</span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Email</label>
                                <div class="col-sm-9">
                                    <span> : {{$user->email}}</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">No Telp</label>
                                <div class="col-sm-9">
                                    <span> : {{$user->no_hp}}</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <label class="control-label col-sm-3">Alamat</label>
                                <div class="col-sm-9">
                                    <span> : {{$user->alamat}}</span>
                                </div>
                            </div>
                            <div class="col-sm-12"><hr></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                @if ($user->avatar == null)
                                    <img src="{{asset('images/ava/avatar.png')}}" alt="" class="img-responsive img-fluid" style="height: 150px; width: 125px">
                                @else
                                    <img src="{{asset('images/ava/'.$user->avatar)}}" alt="" class="img-responsive img-fluid" style="height: 150px; width: 125px">
                                @endif
                                <br>
                                <a href="{{url('myprofile')}}"><< Halaman Profile</a>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Edit Profile</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{url('myprofile/'.$user->id.'/edit')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="_method" value="put">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Nama</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="nama" class="form-control" value="{{$user->nama}}" placeholder="Masukan Nama">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Username</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="username" class="form-control" value="{{$user->username}}" placeholder="Masukan Username">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" name="email" class="form-control" value="{{$user->email}}" placeholder="Masukan Email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" name="password" id="password" class="form-control" placeholder="Masukan Password Baru">
                                        <small>Kosongkan jika tidak ingin mengubah password.</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Ulangi Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" name="confirmation_password" id="confirmation_password" class="form-control" placeholder="Masukan Password Ulang">
                                        <span id="message"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">No Telepon</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="no_hp" class="form-control" value="{{$user->no_hp}}" placeholder="Masukan Nomor Telepon">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea name="alamat" id="" cols="30" rows="3" class="form-control">{{$user->alamat}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Ganti Foto</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="avatar" class="form-control">
                                        <small>Kosongkan jika tidak ingin mengubah foto profile.</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">        
                                    <div class="col-sm-offset-2 col-sm-10">
                                      <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('frontjs')
    <script type="text/javascript">
        $('#password, #confirmation_password').on('keyup', function () {
            if ($('#password').val() == $('#confirmation_password').val()) {
                $('#message').html('Password dapat digunakan!').css('color', 'green');
            } else {
                $('#message').html('Password tidak sama!').css('color', 'red');
            }
        });
    </script>
@endsection