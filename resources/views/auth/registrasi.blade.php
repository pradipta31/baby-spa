<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baby SPA | Register New Account</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @toastr_css
    <link rel="stylesheet" href="{{asset('backend/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/iCheck/square/blue.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box" style="margin-top: 180px">
    <div class="login-logo">
        <a href="{{url('login')}}"><b>Baby SPA</b> Register new Membership</a>
    </div>
    <div class="login-box-body">
        <img src="{{asset('images/logo/logo-angkasa-1.png')}}" alt="" class="img-responsive">
        <br>
        <p class="login-box-msg">Isilah form berikut sesuai dengan data diri anda.</p>
        <form action="{{url('registrasi')}}" method="POST">
            @csrf
            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="nama" value="{{ old('nama')}}" placeholder="Masukan nama anda">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="username" value="{{ old('username')}}" placeholder="Masukan username">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="email" value="{{ old('email')}}" placeholder="Masukan Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="no_hp" value="{{ old('no_hp')}}" placeholder="Masukan No Handphone">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <textarea name="alamat" id="" cols="30" rows="2" class="form-control" placeholder="Masukan alamat anda saat ini">{{old('alamat')}}</textarea>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" id="password" value="{{ old('password')}}" placeholder="Masukan Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="confirmation_password" id="confirmation_password" value="{{ old('confirmation_password')}}" placeholder="Ulangi Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span id="message"></span>
            </div>
            
            <div class="row">
                <div class="col-xs-8">
                    <a href="{{url('login')}}" class="btn btn-default">Halaman Login</a>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block" onclick="loginBtn(this);">
                    Register
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@jquery
@toastr_js
@toastr_render
<script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('backend/plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  });
  function loginBtn(d){
    d.innerHTML = '<i class="fa fa-spinner fa-spin"></i>';
  }

  $('#password, #confirmation_password').on('keyup', function () {
        if ($('#password').val() == $('#confirmation_password').val()) {
            $('#message').html('Password dapat digunakan!').css('color', 'green');
        } else {
            $('#message').html('Password tidak sama!').css('color', 'red');
        }
    });
</script>
</body>
</html>
