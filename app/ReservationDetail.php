<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationDetail extends Model
{
    protected $fillable = [
        'reservation_id',
        'booking_id',
        'keterangan'
    ];

    public function reservation(){
        return $this->belongsTo('App\Reservation');
    }
}
