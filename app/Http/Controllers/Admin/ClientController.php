<?php

namespace App\Http\Controllers\Admin;

use App\Baby;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index(){
        $no = 1;
        $clients = User::where('level', 2)->get();
        return view('admin.client.index', compact('clients', 'no'));
    }

    public function indexBaby(){
        $no = 1;
        $babies = Baby::all();
        return view('admin.baby.index', compact('babies', 'no'));
    }
}