<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Validator;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function create(){
        return view('admin.user.create');
    }

    public function index(){ // index
        $no = 1;
        
        $users = User::where('level', '=', 1)->get(); // nampilin data admin

        return view('admin.user.index', compact('no','users'));
    }

    public function store(Request $r){
        // memvalidasi saat pengisian form
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $u = User::where('username',$r->username)->first();
            if ($u == null) {
                $user = User::create([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'password' => Hash::make($r->password),
                    'level' => 1,
                    'status' => 1
                ]);
                toastSuccess('Admin baru berhasil ditambahkan!');
                return redirect('admin');
            }else{
                toastError('Username sudah digunakan, coba yang lain!');
                return redirect()->back()->withInput();
            }
        }
    }

    public function edit($id){
        $user = User::where('id', $id)->first();
        return view('admin.user.edit', compact('user'));
    }

    public function update(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'status' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password == null) {
                $user = User::where('id', $id)->update([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'status' => $r->status
                ]);
                toastSuccess('Data admin berhasil diubah!');
                return redirect('admin');
            }else{
                $user = User::where('id', $id)->update([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'password' => Hash::make($r->password),
                    'status' => $r->status
                ]);
                toastSuccess('Data admin berhasil diubah!');
                return redirect('admin');
            }
        }
    }

    /*** CLIENT CONTROLLER HERE ***/
    public function editClient($id){
        $user = User::where('id', $id)->first();
        return view('admin.client.edit', compact('user'));
    }

    public function updateClient(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'status' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password == null) {
                $user = User::where('id', $id)->update([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'status' => $r->status
                ]);
                toastSuccess('Data client berhasil diubah!');
                return redirect('admin/client');
            }else{
                $user = User::where('id', $id)->update([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'password' => Hash::make($r->password),
                    'status' => $r->status
                ]);
                toastSuccess('Data client berhasil diubah!');
                return redirect('admin/client');
            }
        }
    }
}
