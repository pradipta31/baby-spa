<?php

namespace App\Http\Controllers\Admin;

use Str;
use Image;
use Validator;
use App\Perawatan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PerawatanController extends Controller
{
    public function create(){
        return view('admin.perawatan.create');
    }

    public function store(Request $r){
        $validator = Validator::make($r->all(), [
            'nama_perawatan' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg|max:5024',
            'keterangan' => 'required',
            'harga' => 'required',
            'status' => 'required',
            'hari_1' => 'required',
            'hari_2' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $foto = $r->file('foto');
            $filename = time() . '.' . $foto->getClientOriginalExtension();
            Image::make($foto)->save(public_path('/images/perawatan/'.$filename));
            $perawatan = Perawatan::create([
                'nama_perawatan' => $r->nama_perawatan,
                'url' => Str::slug($r->nama_perawatan),
                'foto' => $filename,
                'keterangan' => $r->keterangan,
                'jadwal_perawatan' => $r->hari_1.'-'.$r->hari_2,
                'harga' => $r->harga,
                'status' => $r->status
            ]);
            toastSuccess('Berhasil menambahkan perawatan baru!');
            return redirect('admin/perawatan');
        }
    }

    public function index(){
        $no = 1;
        $perawatans = Perawatan::all();
        return view('admin.perawatan.index', compact('no','perawatans'));
    }

    public function edit($id){
        $perawatan = Perawatan::where('id', $id)->first();
        $jadwal = $perawatan->jadwal_perawatan;
        $hari_1 = substr($jadwal, 0, strpos($jadwal, '-'));
        $hari_2 = substr(strstr($jadwal, '-'), strlen('-'));
        // dd($hari_2);
        return view('admin.perawatan.edit', compact('perawatan', 'hari_1', 'hari_2'));
    }

    public function update(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nama_perawatan' => 'required',
            'keterangan' => 'required',
            'status' => 'required',
            'hari_1' => 'required',
            'hari_2' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $perawatan = Perawatan::where('id',$id)->update([
                'nama_perawatan' => $r->nama_perawatan,
                'url' => Str::slug($r->nama_perawatan),
                'keterangan' => $r->keterangan,
                'jadwal_perawatan' => $r->hari_1.'-'.$r->hari_2,
                'harga' => $r->harga,
                'status' => $r->status
            ]);
            toastSuccess('Data jenis perawatan berhasil diubah!');
            return redirect('admin/perawatan');
        }
    }

    public function foto(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'foto' => 'required|image|mimes:jpeg,png,jpg|max:5024',
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $foto = $r->file('foto');
            $filename = time() . '.' . $foto->getClientOriginalExtension();
            $find_img = Perawatan::findOrFail($id);
            unlink(public_path('/images/perawatan/').$find_img->foto);
            Image::make($foto)->save(public_path('/images/perawatan/'.$filename));
            $perawatan = Perawatan::findOrFail($id)->update([
                'foto' => $filename
            ]);

            toastSuccess('Cover foto untuk perawatan '.$find_img->nama_perawatan.' berhasil diganti!');
            return redirect('admin/perawatan');
        }
    }

    public function destroy($id){
        $perawatan = Perawatan::where('id',$id)->delete();
        toastr()->success('Data perawatan yang dipilih berhasil dihapus!');
        return redirect('admin/perawatan');
    }
}
