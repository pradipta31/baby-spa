<?php

namespace App\Http\Controllers\Admin;

use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Validator;
use App\Perawatan;
use App\User;
use App\Baby;
use App\Reservation;
use App\ReservationDetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReservasiController extends Controller
{
    public function index(){
        $no = 1;
        $reservations = Reservation::all();
        return view('admin.reservation.index', compact('reservations', 'no'));
    }

    public function setuju(Request $r, $id){
        $res = Reservation::where('id',$id)->first();
        $baby = Baby::where('id', $res->baby_id)->first();
        $perawatan = Perawatan::where('id',$res->perawatan_id)->first();
        $client = User::where('id',$baby->user_id)->first();

        $code = '#BSPA';
        $random = 1000;
        $i = ++$random;
        $booking_code = $code.''.$i;
        // echo $code.''.$i;
        $res_detail = ReservationDetail::orderBy('id','DESC')->first();
        
        if ($res_detail == null) {
            $reservasi = Reservation::where('id', $id)->update([
                'status_reservasi' => 1
            ]);
            
            $detail = ReservationDetail::insert([
                'reservation_id' => $res->id,
                'booking_id' => $booking_code
            ]);
            try{
                \Mail::send('admin.reservation.approve', [
                    'nama' => $client->nama,
                    'email' => $client->email,
                    'nama_bayi' => $baby->nama_bayi,
                    'perawatan' => $perawatan->nama_perawatan,
                    'harga' => $perawatan->harga,
                    'booking_id' => $booking_code,
                    'tgl_reservasi' => $res->tgl_reservasi
                ], function ($message) use ($client)
                {
                    $message->subject('Hi! '.$client->nama);
                    $message->from('pradiptadipta31@gmail.com', 'Baby SPA Project');
                    $message->to($client->email);
                });
                toastr()->success('Reservasi berhasil disetujui. Email berhasil dikirim ke '.$client->email, '', ['timeOut' => 5000]);
                return redirect()->back();
            }
            catch (Exception $e){
                return response (['status' => false,'errors' => $e->getMessage()]);
            }
        }else{
            $parts = explode('#BSPA',$res_detail->booking_id);
            $im = implode('',$parts);
            $c = ++$im;
            $co = $code.''.$c;
            $reservasi = Reservation::where('id', $id)->update([
                'status_reservasi' => 1
            ]);
            
            $detail = ReservationDetail::insert([
                'reservation_id' => $res->id,
                'booking_id' => $co
            ]);
            try{
                \Mail::send('admin.reservation.approve', [
                    'nama' => $client->nama,
                    'email' => $client->email,
                    'nama_bayi' => $baby->nama_bayi,
                    'perawatan' => $perawatan->nama_perawatan,
                    'harga' => $perawatan->harga,
                    'booking_id' => $co,
                    'tgl_reservasi' => $res->tgl_reservasi
                ], function ($message) use ($client)
                {
                    $message->subject('Hi! '.$client->nama);
                    $message->from('pradiptadipta31@gmail.com', 'Baby SPA Project');
                    $message->to($client->email);
                });
                toastr()->success('Reservasi berhasil disetujui. Email berhasil dikirim ke '.$client->email, '', ['timeOut' => 5000]);
                return redirect()->back();
            }
            catch (Exception $e){
                return response (['status' => false,'errors' => $e->getMessage()]);
            }
        }
    }

    public function tolak(Request $r, $id){
        $res = Reservation::where('id',$id)->first();
        $baby = Baby::where('id', $res->baby_id)->first();
        $perawatan = Perawatan::where('id',$res->perawatan_id)->first();
        $client = User::where('id',$baby->user_id)->first();

        $reservasi = Reservation::where('id', $id)->update([
            'status_reservasi' => 2
        ]);
        
        try{
            \Mail::send('admin.reservation.refuse', [
                'nama' => $client->nama,
                'email' => $client->email,
                'nama_bayi' => $baby->nama_bayi,
                'perawatan' => $perawatan->nama_perawatan,
                'harga' => $perawatan->harga,
                'tgl_reservasi' => $res->tgl_reservasi
            ], function ($message) use ($client)
            {
                $message->subject('Hi! '.$client->nama);
                $message->from('pradiptadipta31@gmail.com', 'Baby SPA Project');
                $message->to($client->email);
            });
            toastr()->error('Reservasi telah ditolak. Email berhasil dikirim ke '.$client->email, '', ['timeOut' => 5000]);
            return redirect()->back();
        }
        catch (Exception $e){
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }

    public function downloadExcel(){
        $spreadsheet = new Spreadsheet();
        $reservasis = Reservation::all();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'REKAP RESERVASI UMAH BABY SPA');
        $sheet->setCellValue('A3', 'No');
        $sheet->setCellValue('B3', 'Tanggal Reservasi');
        $sheet->setCellValue('C3', 'Nama Bayi');
        $sheet->setCellValue('D3', 'Nama Orang Tua');
        $sheet->setCellValue('E3', 'Nama Perawatan');
        $sheet->setCellValue('F3', 'Catatan');
        $sheet->setCellValue('G3', 'Status');

        $row = 4;
        $nomor = 1;
        foreach ($reservasis as $reservasi) {
            $status = '';
            if ($reservasi->status_reservasi == 0) {
                $status = 'Dalam Proses';
            }elseif ($reservasi->status_reservasi == 1) {
                $status = 'Disetujui';
            }elseif ($reservasi->status_reservasi == 2) {
                $status = 'Ditolak';
            }
            $sheet->setCellValue('A'.$row,$nomor++);
            $sheet->setCellValue('B'.$row,$reservasi->tgl_reservasi);
            $sheet->setCellValue('C'.$row,$reservasi->baby->nama_bayi);
            $sheet->setCellValue('D'.$row,$reservasi->baby->user->nama);
            $sheet->setCellValue('E'.$row,$reservasi->perawatan->nama_perawatan);
            $sheet->setCellValue('F'.$row,$reservasi->catatan);
            $sheet->setCellValue('G'.$row,$status);
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('DataReservasi.xlsx');
        return response()->download(public_path('DataReservasi.xlsx'))->deleteFileAfterSend();
    }

    public function downloadPDF($id){
        $reservasi = Reservation::where('id', $id)->first();
        $pdf = PDF::loadview('admin/reservation.print-new',['reservasi'=>$reservasi]);
	    return $pdf->stream('ReservasiClient');
    }
}
