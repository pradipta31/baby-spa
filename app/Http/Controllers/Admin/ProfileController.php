<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Image;
use Validator;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $user = User::where('id', Auth::user()->id)->first();
        return view('admin.profile', compact('user'));
    }

    public function updateAvatar(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user = User::where('id',$id)->first();

            $avatar = $r->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            
            if ($user->avatar != null) {
                unlink(public_path('/images/ava/').$user->avatar);
            }

            Image::make($avatar)->save(public_path('/images/ava/'.$filename));
            
            $user->update([
                'avatar' => $filename
            ]);

            toastSuccess('Update foto profil berhasil');
            return redirect()->back();
        }
    }

    public function updateProfile(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password == null) {
                $user = User::where('id', $id)->first();

                $user->update([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'alamat' => $r->alamat,
                    'no_hp' => $r->no_hp
                ]);

                toastSuccess('Update data profile berhasil!');
                return redirect()->back();
            } else {
                if ($r->password != $r->confirmation_password) {
                    toastError('Password tidak sama!');
                    return redirect()->back();
                } else {
                    $user = User::where('id', $id)->first();

                    $user->update([
                        'nama' => $r->nama,
                        'username' => $r->username,
                        'email' => $r->email,
                        'password' => Hash::make($r->password),
                        'alamat' => $r->alamat,
                        'no_hp' => $r->no_hp
                    ]);

                    toastSuccess('Update data profile berhasil!');
                    return redirect()->back();
                }
                
            }
            
        }
    }
}
