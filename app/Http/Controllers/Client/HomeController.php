<?php

namespace App\Http\Controllers\Client;

use Auth;
use App\User;
use App\Perawatan;
use App\Baby;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $perawatans = Perawatan::where('status', 'published')->paginate(3);
        return view('client.home', compact('perawatans'));
    }

    public function perawatan(){
        $perawatans = Perawatan::where('status', 'published')->paginate(6);
        return view('client.perawatan', compact('perawatans'));
    }

    public function detailPerawatan($slug){
        $perawatan = Perawatan::where('url',$slug)->first();
        return view('client.detail_perawatan', compact('perawatan'));
    }

    public function formBook($id){
        $perawatan = Perawatan::where('id', $id)->first();
        $jadwal = $perawatan->jadwal_perawatan;
        $hari_1 = substr($jadwal, 0, strpos($jadwal, '-'));
        $hari_2 = substr(strstr($jadwal, '-'), strlen('-'));

        $jadwal1 = 0;
        if ($hari_1 == 'Senin' OR $hari_1 == 'senin') {
            $jadwal1 = 1;
        }elseif ($hari_1 == 'Selasa' OR $hari_1 == 'selasa') {
            $jadwal1 = 2;
        }elseif ($hari_1 == 'Rabu' OR $hari_1 == 'rabu') {
            $jadwal1 = 3;
        }elseif ($hari_1 == 'Kamis' OR $hari_1 == 'kamis') {
            $jadwal1 = 4;
        }elseif ($hari_1 == 'Jumat' OR $hari_1 == 'jumat') {
            $jadwal1 = 5;
        }elseif ($hari_1 == 'Sabtu' OR $hari_1 == 'sabtu') {
            $jadwal1 = 6;
        }elseif ($hari_1 == 'Minggu' OR $hari_1 == 'minggu') {
            $jadwal1 = 0;
        }

        $jadwal2 = 0;
        if ($hari_2 == 'Senin' OR $hari_2 == 'senin') {
            $jadwal2 = 1;
        }elseif ($hari_2 == 'Selasa' OR $hari_2 == 'selasa') {
            $jadwal2 = 2;
        }elseif ($hari_2 == 'Rabu' OR $hari_2 == 'rabu') {
            $jadwal2 = 3;
        }elseif ($hari_2 == 'Kamis' OR $hari_2 == 'kamis') {
            $jadwal2 = 4;
        }elseif ($hari_2 == 'Jumat' OR $hari_2 == 'jumat') {
            $jadwal2 = 5;
        }elseif ($hari_2 == 'Sabtu' OR $hari_2 == 'sabtu') {
            $jadwal2 = 6;
        }elseif ($hari_2 == 'Minggu' OR $hari_2 == 'minggu') {
            $jadwal2 = 0;
        }


        // dd($jadwal1.'-'.$jadwal2);
        $babies = Baby::where('user_id', Auth::user()->id)->get();
        return view('client.auth.form-booking', compact('perawatan', 'babies'));
    }    
}
