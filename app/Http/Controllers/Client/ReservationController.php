<?php

namespace App\Http\Controllers\Client;

use Validator;
use Auth;
use App\Baby;
use App\Reservation;
use App\Perawatan;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function bookNow(Request $r){
        $validator = Validator::make($r->all(), [
            'baby_id' => 'required',
            'tgl_reservasi' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $tanggal = date('Y-m-d', strtotime($r->tgl_reservasi));
            $reservation = Reservation::create([
                'baby_id' => $r->baby_id,
                'perawatan_id' => $r->perawatan_id,
                'tgl_reservasi' => $tanggal,
                'catatan' => $r->catatan,
                'status_reservasi' => 0
            ]);
            toastSuccess('Pemesanan anda berhasil di proses, periksa status pemesanan pada menu reservasi!');
            return redirect('reservation-on-proccess');
        }
    }

    public function proccess(){
        return view('client.auth.reservation.success');
    }

    public function reservation(){
        $reservations = Reservation::select('reservations.*', 'babies.*', 'users.*')
                        ->join('babies', 'babies.id', '=', 'reservations.baby_id')
                        ->join('users', 'users.id', '=', 'babies.user_id')
                        ->where('users.id', '=', Auth::user()->id)
                        ->get();
        // dd($reservations);
        $no = 1;
        return view('client.auth.reservation.my-reservation', compact('reservations', 'no'));

    }
}
