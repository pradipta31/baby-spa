<?php

namespace App\Http\Controllers\Client;

use Hash;
use Image;
use Validator;
use Auth;
use App\User;
use App\Baby;
use App\Perawatan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function myProfile(){
        $user = User::where('id',Auth::user()->id)->first();
        $babies = Baby::where('user_id', $user->id)->get();
        $no = 1;
        return view('client.auth.profile.myprofile', compact('user', 'babies', 'no'));
    }

    public function editProfile(){
        $user = User::where('id', Auth::user()->id)->first();
        return view('client.auth.profile.editprofile', compact('user'));
    }

    public function updateProfile(Request $r, $id, User $user){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'no_hp' => 'required|numeric',
            'alamat' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password == null) {
                if ($r->hasFile('avatar')) {
                    $v = Validator::make($r->all(), [
                        'avatar' => 'required|image|mimes:jpeg,png,jpg|max:5024',
                    ]);
                    if ($v->fails()) {
                        toastError($v->messages()->first());
                        return redirect()->back()->withInput();
                    }else{
                        $avatar = $r->file('avatar');
                        $filename = time() . '.' . $avatar->getClientOriginalExtension();
                        Image::make($avatar)->save(public_path('/images/ava/'.$filename));
                        $user = User::where('id',$id)->update([
                            'nama' => $r->nama,
                            'username' => $r->username,
                            'email' => $r->email,
                            'avatar' => $filename,
                            'no_hp' => $r->no_hp,
                            'alamat' => $r->alamat
                        ]);
                        toastSuccess('Berhasil mengubah profile!');
                        return redirect()->back();
                    }
                }else{
                    $user = User::where('id',$id)->update([
                        'nama' => $r->nama,
                        'username' => $r->username,
                        'email' => $r->email,
                        'no_hp' => $r->no_hp,
                        'alamat' => $r->alamat
                    ]);
                    toastSuccess('Berhasil mengubah profile!');
                    return redirect()->back();
                }
            }else{
                if ($r->password != $r->confirmation_password) {
                    toastError('Password yang anda masukkan tidak sama! Silahkan ulangi!');
                    return redirect()->back()->withInput();
                }else{
                    if ($r->hasFile('avatar')) {
                        $v = Validator::make($r->all(), [
                            'avatar' => 'required|image|mimes:jpeg,png,jpg|max:5024',
                        ]);
                        if ($v->fails()) {
                            toastError($v->messages()->first());
                            return redirect()->back()->withInput();
                        }else{
                            $avatar = $r->file('avatar');
                            $filename = time() . '.' . $avatar->getClientOriginalExtension();
                            Image::make($avatar)->save(public_path('/images/ava/'.$filename));
                            $user = User::where('id',$id)->update([
                                'nama' => $r->nama,
                                'username' => $r->username,
                                'email' => $r->email,
                                'avatar' => $filename,
                                'password' => Hash::make($r->password),
                                'no_hp' => $r->no_hp,
                                'alamat' => $r->alamat
                            ]);
                            toastSuccess('Berhasil mengubah profile!');
                            return redirect()->back();
                        }
                    }else{
                        $user = User::where('id',$id)->update([
                            'nama' => $r->nama,
                            'username' => $r->username,
                            'email' => $r->email,
                            'password' => Hash::make($r->password),
                            'no_hp' => $r->no_hp,
                            'alamat' => $r->alamat
                        ]);
                        toastSuccess('Berhasil mengubah profile!');
                        return redirect()->back();
                    }
                }
            }
        }
    }

    public function newBaby(){
        $perawatan = NULL;
        return view('client.auth.profile.create-baby', compact('perawatan'));
    }

    public function storeBaby(Request $r){
        $validator = Validator::make($r->all(), [
            'nama_bayi' => 'required',
            'umur' => 'required',
            'tgl_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'tinggi_badan' => 'required',
            'berat_badan' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user_id = $r->id_user;
            $birthday = date('Y-m-d', strtotime($r->tgl_lahir));
            // dd($birthday);

            if ($r->hasFile('foto')) {
                $v = Validator::make($r->all(), [
                    'foto' => 'required|image|mimes:jpeg,png,jpg|max:5024',
                ]);
                if ($v->fails()) {
                    toastError($v->messages()->first());
                    return redirect()->back()->withInput();
                }else{
                    $foto = $r->file('foto');
                    $filename = time() . '.' . $foto->getClientOriginalExtension();
                    Image::make($foto)->save(public_path('/images/baby/'.$filename));
                    $babies = Baby::create([
                        'user_id' => $user_id,
                        'nama_bayi' => $r->nama_bayi,
                        'umur' => $r->umur,
                        'tgl_lahir' => $birthday,
                        'jenis_kelamin' => $r->jenis_kelamin,
                        'foto' => $filename,
                        'tb_bb' => $r->tinggi_badan.'/'.$r->berat_badan
                    ]);
                    toastSuccess('Berhasil menyimpan data!');
                    if ($r->id_perawatan) {
                        return redirect(url('perawatan/book-now/'.$r->id_perawatan));
                    } else {
                        return redirect('myprofile');
                    }
                    
                }
            }else{
                $babies = Baby::create([
                    'user_id' => $user_id,
                    'nama_bayi' => $r->nama_bayi,
                    'umur' => $r->umur,
                    'tgl_lahir' => $birthday,
                    'jenis_kelamin' => $r->jenis_kelamin,
                    'tb_bb' => $r->tinggi_badan.'/'.$r->berat_badan
                ]);
                toastSuccess('Berhasil menyimpan data!');
                if ($r->id_perawatan) {
                    return redirect(url('perawatan/book-now/'.$r->id_perawatan));
                } else {
                    return redirect('myprofile');
                }
                
            }
        }
    }

    public function editBaby($id){
        $baby = Baby::where('id',$id)->first();
        $tgl_lahir = date('d/m/Y', strtotime($baby->tgl_lahir));

        $tb_bb = $baby->tb_bb;
        $tb = substr($tb_bb, 0, strpos($tb_bb, '/'));
        $bb = substr(strstr($tb_bb, '/'), strlen('/'));
        return view('client.auth.profile.editbaby', compact('baby', 'tgl_lahir', 'tb', 'bb'));
    }

    public function updateBaby(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nama_bayi' => 'required',
            'umur' => 'required',
            'tgl_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'tinggi_badan' => 'required',
            'berat_badan' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $birthday = date('Y-m-d', strtotime($r->tgl_lahir));
            // dd($birthday);

            if ($r->hasFile('foto')) {
                $v = Validator::make($r->all(), [
                    'foto' => 'required|image|mimes:jpeg,png,jpg|max:5024',
                ]);
                if ($v->fails()) {
                    toastError($v->messages()->first());
                    return redirect()->back()->withInput();
                }else{
                    $foto = $r->file('foto');
                    $filename = time() . '.' . $foto->getClientOriginalExtension();
                    Image::make($foto)->save(public_path('/images/baby/'.$filename));
                    $babies = Baby::where('id',$id)->update([
                        'nama_bayi' => $r->nama_bayi,
                        'umur' => $r->umur,
                        'tgl_lahir' => $birthday,
                        'jenis_kelamin' => $r->jenis_kelamin,
                        'foto' => $filename,
                        'tb_bb' => $r->tinggi_badan.'/'.$r->berat_badan
                    ]);
                    toastSuccess('Berhasil menyimpan data!');
                    return redirect('myprofile');
                }
            }else{
                $babies = Baby::where('id',$id)->update([
                    'nama_bayi' => $r->nama_bayi,
                    'umur' => $r->umur,
                    'tgl_lahir' => $birthday,
                    'jenis_kelamin' => $r->jenis_kelamin,
                    'tb_bb' => $r->tinggi_badan.'/'.$r->berat_badan
                ]);
                toastSuccess('Berhasil menyimpan data!');
                return redirect('myprofile');
            }
        }
    }

    // RESERVATION NEW BABY
    public function newBabyR($id){
        $perawatan = $id;
        return view('client.auth.profile.create-baby', compact('perawatan'));
    }
}
