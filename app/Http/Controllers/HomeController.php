<?php

namespace App\Http\Controllers;

use Auth;
use App\Perawatan;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->level == 1) {
            return view('admin.dashboard');
        }else{
            toastSuccess('Login berhasil!');
            $perawatans = Perawatan::where('status', 'published')->paginate(3);
            return view('client.home', compact('perawatans'));
        }
    }
}
