<?php

namespace App\Http\Controllers;

use Hash;
use Validator;
use App\User;
use Illuminate\Http\Request;

class RegistrasiController extends Controller
{
    public function index(){
        return view('auth.registrasi');
    }

    public function post(Request $r){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required',
            'password' => 'required',
            'no_hp' => 'required|numeric',
            'alamat' => 'required'
        ]);
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $email = User::where('email',$r->email)->count();
            if ($r->password != $r->confirmation_password) {
                toastError('Password konfirmasi tidak sama mohon ulangi!');
                return redirect()->back()->withInput();
            } else {
                if ($email == 1) {
                    toastError('Email sudah digunakan!');
                    return redirect()->back()->withInput();
                }else{
                    $random = md5(uniqid(rand(), true));
                    $user = User::create([
                        'nama' => $r->nama,
                        'username' => $r->username,
                        'email' => $r->email,
                        'password' => Hash::make($r->password),
                        'no_hp' => $r->no_hp,
                        'alamat' => $r->alamat,
                        'level' => 2,
                        'status' => 0
                    ]);
                    try{
                        \Mail::send('email', [
                            'nama' => $r->nama,
                            'username' => $r->username,
                            'email' => $r->email
                        ], function ($message) use ($r)
                        {
                            $message->subject('Hi! '.$r->nama);
                            $message->from('pradiptadipta31@gmail.com', 'Baby SPA Project');
                            $message->to($r->email);
                        });
                        toastr()->success('Email berhasil terkirim ke '.$r->email.' Harap untuk konfirmasi email', '', ['timeOut' => 5000]);
                        return redirect()->back();
                    }
                    catch (Exception $e){
                        return response (['status' => false,'errors' => $e->getMessage()]);
                    }
                }
            }
            
        }
    }

    public function setup($username){
        // dd($username);
        $user = User::where('username',$username)->first();
        return view('success', compact('user'));
    }

    public function verification(Request $r, $id){
        $now = new \DateTime();
        $user = User::where('id', $id)->update([
            'email_verified_at' => $now,
            'status' => 1
        ]);
        toastSuccess('Akun berhasil di verifikasi, silahkan melakukan login!');
        return redirect('login');
    }
}
