<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'baby_id',
        'perawatan_id',
        'tgl_reservasi',
        'catatan',
        'status_reservasi'
    ];

    public function baby(){
        return $this->belongsTo('App\Baby');
    }

    public function perawatan(){
        return $this->belongsTo('App\Perawatan');
    }
}
