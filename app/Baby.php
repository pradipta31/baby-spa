<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Baby extends Model
{
    protected $fillable = [
        'user_id',
        'nama_bayi',
        'umur',
        'tgl_lahir',
        'jenis_kelamin',
        'foto',
        'tb_bb'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
