<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perawatan extends Model
{
    protected $fillable = [
        'nama_perawatan',
        'url',
        'foto',
        'keterangan',
        'jadwal_perawatan',
        'harga',
        'status'
    ];
}
